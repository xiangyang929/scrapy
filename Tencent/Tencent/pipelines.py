# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import json


class TencentPipeline(object):
    # 主要初始化打开一个文件夹，并表示将要向其中输入数据。只执行一次
    def __init__(self):
        self.f = open("detail.json", 'w')

    # 主要用于把json格式转化为unicode编码，并把数据写入文件中。
    def process_item(self, item, spider):
        content = json.dumps(dict(item), ensure_ascii=False) + ",\n"
        self.f.write(content)
        return item

    #  用于写数据结束后关闭文件
    def close_spider(self, spider):
        self.f.close()
