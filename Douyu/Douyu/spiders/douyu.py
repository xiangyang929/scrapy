# -*- coding: utf-8 -*-
import json

import scrapy
from Douyu.items import DouyuItem


class DouyuSpider(scrapy.Spider):
    name = 'douyu'
    allowed_domains = ['douyucdn.cn']
    baseURL = 'http://capi.douyucdn.cn/api/v1/live?limit=20&offset='
    offset = 0
    start_urls = [baseURL + str(offset)]

    def parse(self, response):
        data_list = json.loads(response.body)['data']
        if len(data_list) == 0:
            return
        for data in data_list:
            item = DouyuItem()
            item['imagelink'] = data['vertical_src']
            item['nickname'] = data['nickname']
            yield item

        self.offset += 20
        url = self.baseURL + str(self.offset)
        yield scrapy.Request(url, callback=self.parse)